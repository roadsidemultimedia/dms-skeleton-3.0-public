# Purpose
The purpose of the repository is to enable public download of release zips.

See [Downloads page](https://bitbucket.org/roadsidemultimedia/dms-skeleton-3.0-public/downloads/) for zip files